title addition of 3 numbers in byte format
include \masm32\include\masm32rt.inc
.data
a1 db 0ffh,0 ;
i db 0 ;
b db 2 ;
c1 db 3 ;
d db 3 ;
res dd 0,0 ;
titl db "Transforming of first lab to MASM: ",0; simplified window name
st1 dq 1 dup(0),0 ; message output buffer
ifmt db "Displaying variants to calculate formula: ",0ah,
"i = 2", 10, "b = 2",10, "c = 3",10,"d = c",10,"Result: ",10,
"res = i+b+c-d = %x",0ah,0ah
.code
entry_point proc
xor al,al ; zeroing - so as not to see garbage
mov al, [b]                                      ;res = i + b + c - d;
               ;convert string to decimal
mov [i], al

mov cl, [c1]

mov dl, cl

add al, al
add al, [c1]
sub al, [d]

mov [res], eax


movzx edx,dx ; extension (can be omitted if there is no garbage)
invoke wsprintf, ADDR st1, ADDR ifmt,edx;
invoke MessageBox,0,addr st1,addr titl, MB_OK
invoke ExitProcess,0
entry_point endp
End
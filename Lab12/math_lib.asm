include \masm32\include\masm32rt.inc

.data
hinst dd 0
res db 0
titl db "Output via function MessageBox",0				; simplified window name
st1 dq 1 dup(0),0 ; message output buffer
ifmt  db "Result: %d",0ah,0ah
.code
DllEntry proc instance:HINSTANCE, reason:DWORD, unused:DWORD	;template entry point function
	mov eax,reason
	cmp eax,DLL_PROCESS_ATTACH
	jne _exit
	push instance
	pop hinst
_exit:
	and eax,1
	ret
DllEntry endp

division proc var1:WORD, var2:BYTE				;division procedure with 2 arguments
xor eax,eax 							        ;zeroing - so as not to see garbage

mov ax, var1
mov bl, var2
div bl

mov res, al

invoke wsprintf, ADDR st1, ADDR ifmt,res;

invoke MessageBox, 0,addr st1,addr titl, MB_OK
ret								; function exit

division endp

addition proc var1:WORD, var2:BYTE
xor eax,eax 							; zeroing - so as not to see garbage

mov ax, var1
mov bl, var2
add ax, bx
mov res, al
invoke wsprintf, ADDR st1, ADDR ifmt,res;

invoke MessageBox, 0,addr st1,addr titl, MB_OK
ret								; function exit

addition endp 

subtraction proc var1:WORD, var2:BYTE
xor eax,eax 							; zeroing - so as not to see garbage

mov ax, var1
mov bl, var2
sub ax, bx
mov res, al
invoke wsprintf, ADDR st1, ADDR ifmt,res;

invoke MessageBox, 0,addr st1,addr titl, MB_OK
ret								; function exit

subtraction endp 

multiplication proc var1:WORD, var2:BYTE
xor eax,eax 							; zeroing - so as not to see garbage

mov ax, var1
mov bl, var2
mul bl
mov res, al
invoke wsprintf, ADDR st1, ADDR ifmt,res;

invoke MessageBox, 0,addr st1,addr titl, MB_OK
ret								; function exit

multiplication endp 
end

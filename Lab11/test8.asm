include \masm32\include\masm32rt.inc
;a - e + c - a * b
.MMX
.data ;
titl db "Output via function MessageBox",0; simplified window name
st1 dq 1 dup(0),0 ; message output buffer
ifmt db "Answer %d",0
a dd 7
b dd 6
c1 dd 2
e dd 2
.code
entry_point proc
mov eax, a
mov ebx, b
mov ecx, c1
mov edx, e
movd mm0, eax
movd mm1, ebx
cmp eax, ebx
jg l1
pmullw mm0, mm1; ;if(a < b) calculate a * b; else calculate (a - e + c - a * b)
movd eax, mm0
jp l2
l1:
movd mm2, edx
movd mm3, ecx
movd mm4, eax
psubb mm0, mm2 ; a = a - e
paddd mm0, mm3 ; a = a + c
pmullw mm4, mm1; mm4 = a * b
psubd mm0, mm4
movd eax, mm0
invoke wsprintf, ADDR st1, ADDR ifmt, eax;
invoke MessageBox, 0,addr st1,addr titl, MB_OK
jp l3
l2:
invoke wsprintf, ADDR st1, ADDR ifmt, eax;
invoke MessageBox, 0,addr st1,addr titl, MB_OK
l3:
invoke ExitProcess, 0
entry_point endp
end

NULL              EQU 0                         ; Constants
STD_OUTPUT_HANDLE EQU -11
extern _GetStdHandle@4                          ; Import external symbols
extern _WriteFile@20                           ; Windows API functions, decorated
extern _ExitProcess@4
global Start                                    ; Export symbols. The entry point
section .data                                   ; Initialized data segment
 Message        db "Result : ", 0Dh, 0Ah
 MessageLength  EQU $-Message                   ; Address of this line ($) - address of Message
 i db 0
 b db '2'
 c db '3'
 d db '3'
 section .bss                                    ; Uninitialized data segment
 StandardHandle resd 1
 Written        resd 1
 res resb 1
section .text                                   ; Code segment
Start:
mov al, [b]                                      ;res = i + b + c - d;
sub al, '0'               ;convert string to decimal
mov [i], al

mov cl, [c]
sub cl, '0'
mov dl, cl

add al, al
add al, [c]
sub al, [d]
add al, '0'
mov [res], al
 push  STD_OUTPUT_HANDLE
 call  _GetStdHandle@4
 mov   dword [StandardHandle], EAX

	push  NULL                                     ; 5th parameter
 push  Written                                  ; 4th parameter
 push  MessageLength                            ; 3rd parameter
 push  Message                                  ; 2nd parameter
 push  dword [StandardHandle]                   ; 1st parameter
 call  _WriteFile@20                            ; Output can be redirect to a file using >

 push  NULL                                     ; 5th parameter
 push  Written                                  ; 4th parameter
 push  1                           ; 3rd parameter
 push  res                                  ; 2nd parameter
 push  dword [StandardHandle]                   ; 1st parameter
 call  _WriteFile@20                            ; Output can be redirect to a file using >

 push  NULL
 call  _ExitProcess@4
 
#include "stdio.h"
int main()
{
	short  i,b = 2, c = 3, d, res;
	/* translate this code to assembly language
	i = b;
	d = c,
	res = i + b + c - d;
	*/

	__asm {
		mov AX,b
		mov  i,AX
		mov BX, b
		mov CX,c
		mov DX,d
		add AX,BX
		mov c,CX
		add CX,b
		mov CX,DX
		sub CX,DX
		mov res,AX
	}
	printf("%d", res);
}

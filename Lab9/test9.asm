NULL              EQU 0                         ; Constants
extern _ExitProcess@4
extern Beep
extern Sleep

global Start                                    ; Export symbols. The entry point

section .data
section .bss
section .text                                   ; Code segment
Start:

push  500
push  659
call Beep

push  500
push  659
call Beep

push 500
push 659
call Beep

push  500
call Sleep
	
push  500
push  659
call Beep

push  500
push  784
call Beep

push 500
push 523
call Beep

push  500
push  587
call Beep

push  500
push  659
call Beep

push  500
push  261
call Beep

push  500
push  293
call Beep

push  500
push  329
call Beep

push  500
push  698
call Beep

push  500
push  698
call Beep

push  500
push  698
call Beep

push  500
push  698
call Beep

push 500
call Sleep

push  500
push  698
call Beep

push  500
push  659
call Beep

push  500
push  659
call Beep

push 500
call Sleep

push  500
push  659
call Beep

push  500
push  587
call Beep

push  500
push  587
call Beep

push  500
push  659
call Beep

push  500
push  587
call Beep

push 500
call Sleep

push  NULL
call  _ExitProcess@4

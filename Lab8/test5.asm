NULL EQU 0 ; Constants
STD_OUTPUT_HANDLE EQU -11
global Start
extern _ExitProcess@4
extern printf ;from msvcrt
section .bss
name: resb 100
section .data
struc university
 .student_name: resq 1
 .scholarship: resq 1
 .grade: resq 1
 .age: resq 1
endstruc
str1:
 istruc university
 at university.student_name 
 dd "Harvi"
 at university.scholarship
 dd 2400
 at university.grade
 dd 3
 at university.age
 dd 35
 iend
str2:
 istruc university
 at university.student_name
 dd "Maik"
 at university.scholarship
 dd 2100
 at university.grade
 dd 2
 at university.age
 dd 25
 iend
str3:
 istruc university
 at university.student_name
 dd "Raychel"
 at university.scholarship
 dd 1800
 at university.grade
 dd 7
 at university.age
 dd 21
 iend
str4:
 istruc university
 at university.student_name
 dd "Dona"
 at university.scholarship
 dd 1500
 at university.grade
 dd 2
 at university.age
 dd 27
 iend

titl1 dq "The average_age of the students: "
temp db "%d",0ah,0
need_stop db 0
section .text
Start:
push titl1
call printf
mov eax, [str1 + university.age] ;eax holds the sum of ages
add eax, [str2 + university.age]
add eax, [str3 + university.age]
add eax, [str4 + university.age]
mov ebx, 4
div ebx
push eax
push temp
call printf
push NULL
call _ExitProcess@4